package cymru.prv.dictionary.irish;

import cymru.prv.dictionary.common.DictionaryList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.ThrowingSupplier;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class TestImports {

    @Test
    public void testInitiatesWithoutThrowing(){
        Assertions.assertDoesNotThrow(
                (ThrowingSupplier<IrishDictionary>) IrishDictionary::new
        );
    }

    @Test
    public void testInitiatesWithoutThrowingWithList(){
        Assertions.assertDoesNotThrow(() -> {
            DictionaryList list = new DictionaryList();
            new IrishDictionary(list);
        });
    }

    @Test
    public void ensureThatVersionNumbersAreTheSame() throws IOException {
        for(var line : Files.readAllLines(Path.of( "build.gradle"))){
            if(line.startsWith("version")){
                String version = line
                        .split("\\s+")[1]
                        .replace("'", "");
                Assertions.assertEquals(version, new IrishDictionary().getVersion());
                return;
            }
        }
        Assertions.fail("Version number not found in build.gradle");
    }

}
