package cymru.prv.dictionary.irish.tenses;

import cymru.prv.dictionary.irish.IrishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class FirstImperfectIrishVerbTense extends LenitedIrishVerbTense {

    public FirstImperfectIrishVerbTense(IrishVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.singletonList(applyBroadOrSlender("ainn", "inn"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Collections.singletonList(applyBroadOrSlender("tá", "teá"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return getDefaultAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(applyBroadOrSlender("aimis", "imis"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return getDefaultAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Collections.singletonList(applyBroadOrSlender("aidis", "idis"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlender("taí", "tí"));
    }

    @Override
    protected List<String> getDefaultAnalytic() {
        return Collections.singletonList(applyBroadOrSlender("adh", "eadh"));
    }
}
