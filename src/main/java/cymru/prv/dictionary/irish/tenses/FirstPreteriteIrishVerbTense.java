package cymru.prv.dictionary.irish.tenses;

import cymru.prv.dictionary.irish.IrishLenition;
import cymru.prv.dictionary.irish.IrishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class FirstPreteriteIrishVerbTense extends LenitedIrishVerbTense {

    public FirstPreteriteIrishVerbTense(IrishVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultAnalytic() {
        if(hasLenition)
            return Collections.singletonList(IrishLenition.preformLenition(stem));
        return Collections.singletonList(stem);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(applyBroadOrSlender("amar", "eamar"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlender("adh", "eadh"));
    }
}
