package cymru.prv.dictionary.irish.tenses;

import cymru.prv.dictionary.irish.IrishLenition;
import cymru.prv.dictionary.irish.IrishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class SpecialIrishVerbTense extends LenitedIrishVerbTense {

    public SpecialIrishVerbTense(IrishVerb verb, JSONObject obj) {
        super(verb, obj, false);
    }

    @Override
    protected List<String> getDefaultAnalytic() {
        if(defaults != null)
            return getDefaultsOrAnalytic();
        if(hasLenition)
            return Collections.singletonList(IrishLenition.preformLenition(stem));
        return Collections.singletonList(stem);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return getDefaultsOrAnalytic();
    }
}
