package cymru.prv.dictionary.irish.tenses;

import cymru.prv.dictionary.irish.IrishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class FirstConditionalIrishVerbTense extends LenitedIrishVerbTense {

    public FirstConditionalIrishVerbTense(IrishVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultAnalytic() {
        return Collections.singletonList(applyBroadOrSlender("fadh", "feadh"));
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.singletonList(applyBroadOrSlender("fainn", "finn"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Collections.singletonList(applyBroadOrSlender("fá", "feá"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(applyBroadOrSlender("faimis", "fimis"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Collections.singletonList(applyBroadOrSlender("faidís", "fidís"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlender("faí", "fí"));
    }
}
