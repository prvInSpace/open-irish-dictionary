package cymru.prv.dictionary.irish.tenses;

import cymru.prv.dictionary.irish.IrishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class SecondConditionalIrishVerbTense extends LenitedIrishVerbTense {

    public SecondConditionalIrishVerbTense(IrishVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultAnalytic() {
        return Collections.singletonList(applyBroadOrSlender("ódh", "oedh"));
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.singletonList(applyBroadOrSlender("óinn", "eionn"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Collections.singletonList(applyBroadOrSlender("ófa", "eófa"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return getDefaultAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(applyBroadOrSlender("óimis", "eoimis"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return getDefaultAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Collections.singletonList(applyBroadOrSlender("óidís", "eoidís"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlender("ófaí", "eofaí"));
    }
}
