package cymru.prv.dictionary.irish.tenses;

import cymru.prv.dictionary.irish.IrishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class FirstFutureIrishVerbTense extends IrishVerbTense {

    public FirstFutureIrishVerbTense(IrishVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultAnalytic() {
        return Collections.singletonList(applyBroadOrSlender("faidh", "fidh"));
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(applyBroadOrSlender("faimid", "fimid"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return getDefaultsOrAnalytic();
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlender("far", "fear"));
    }
}
