package cymru.prv.dictionary.irish.tenses;

import cymru.prv.dictionary.irish.IrishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class SecondPresentIrishVerbTense extends IrishVerbTense {

    public SecondPresentIrishVerbTense(IrishVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.singletonList(applyBroadOrSlender("aím", "ím"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return this.getDefaultAnalytic();
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return this.getDefaultAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(applyBroadOrSlender("aímid", "ímid"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return this.getDefaultAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return this.getDefaultAnalytic();
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlender("aítear", "ítear"));
    }

    @Override
    protected List<String> getDefaultAnalytic() {
        return Collections.singletonList(applyBroadOrSlender("aíonn", "íonn"));
    }
}
