package cymru.prv.dictionary.irish.tenses;

import cymru.prv.dictionary.irish.IrishLenition;
import cymru.prv.dictionary.irish.IrishVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;

public class SecondPreteriteIrishVerbTense extends LenitedIrishVerbTense {

    private final String normalForm;

    public SecondPreteriteIrishVerbTense(IrishVerb verb, JSONObject obj) {
        super(verb, obj);
        this.normalForm = verb.getNormalForm();
    }

    @Override
    protected List<String> getDefaultAnalytic() {
        return Collections.singletonList(IrishLenition.preformLenition(normalForm));
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return getDefaultAnalytic();
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return getDefaultAnalytic();
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return getDefaultAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(applyBroadOrSlender("aíomar","íomar"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return getDefaultAnalytic();
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return getDefaultAnalytic();
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(applyBroadOrSlenderWithoutLenition("aíodh", "íodh"));
    }
}
