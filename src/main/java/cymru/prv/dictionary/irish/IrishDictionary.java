package cymru.prv.dictionary.irish;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.DictionaryList;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONObject;

import java.util.Map;
import java.util.function.Function;

/**
 * Represents an Irish dictionary
 *
 * Creates a dictionary with the code "ga"
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class IrishDictionary extends Dictionary {

    private static final String languageName = "Irish";
    private static final String languageCode = "ga";
    private static final Map<WordType, Function<JSONObject, Word>> wordTypeFromJsonMap = Map.of(
            WordType.verb, IrishVerb::new,
            WordType.noun, IrishNoun::new,
            WordType.adposition, IrishAdposition::new,
            WordType.adjective, (w) -> new Word(w, WordType.adjective),
            WordType.conjunction, (w) -> new Word(w, WordType.conjunction)
    );

    public IrishDictionary(){
        super(languageCode, wordTypeFromJsonMap);
    }

    public IrishDictionary(DictionaryList dictionaryList) {
        super(dictionaryList, languageCode, wordTypeFromJsonMap);
    }


    @Override
    public String getVersion() {
        return "1.2.0";
    }
}
