package cymru.prv.dictionary.irish;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import cymru.prv.dictionary.common.json.Json;
import org.json.JSONObject;

import java.util.*;


/**
 * Represents a noun in Irish
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class IrishNoun extends Word {

    private static final String PLURAL = "plural";
    private static final List<String> NOUNCASES = Arrays.asList(
      "vocative",
      "genitive",
      "dative"
    );

    private final List<String> plural;
    private final Map<String, NounCase> cases = new HashMap<>();

    public IrishNoun(JSONObject obj) {
        super(obj, WordType.noun);
        plural = Json.getStringListOrNull(obj, PLURAL);
        for(String caseName : NOUNCASES)
            if(obj.has(caseName))
                cases.put(caseName, new NounCase(obj.getJSONObject(caseName)));
    }

    @Override
    protected JSONObject getInflections() {
        JSONObject obj = new JSONObject();
        if(plural != null)
            obj.put(PLURAL, plural);
        for(String caseName : cases.keySet())
            obj.put(caseName, cases.get(caseName).toJson());
        return obj;
    }

    @Override
    public List<String> getVersions() {
        var list = super.getVersions();
        if(plural != null)
            list.addAll(plural);
        for(NounCase nounCase : cases.values())
            list.addAll(nounCase.getVersions());
        return list;
    }
}
