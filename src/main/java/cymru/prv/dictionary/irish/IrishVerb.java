package cymru.prv.dictionary.irish;

import cymru.prv.dictionary.common.*;
import cymru.prv.dictionary.irish.tenses.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;


/**
 * Represents a verb in Irish
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class IrishVerb extends Word {

    public static final String PRESENT = "present";
    public static final String PRETERITE = "preterite";
    public static final String IMPERATIVE = "imperative";
    public static final String IMPERFECT = "imperfect";
    public static final String FUTURE = "future";
    public static final String CONDITIONAL = "conditional";
    public static final String PRESENT_INDEPENDENT = "present_independent";
    public static final String PRESENT_DEPENDENT = "present_dependent";
    public static final String PRESENT_NEGATIVE = "present_negative";
    public static final String PRESENT_HABITUAL = "present_habitual";
    public static final String PAST_INDEPENDENT = "past_independent";
    public static final String PAST_DEPENDENT = "past_dependent";
    public static final String PAST_HABITUAL = "past_habitual";
    public static final String FUTURE_INDEPENDENT = "future_independent";
    public static final String FUTURE_DEPENDENT = "future_dependent";
    public static final String CONDITIONAL_INDEPENDENT = "conditional_independent";
    public static final String CONDITIONAL_DEPENDENT = "conditional_dependent";

    private enum ConjugationForm {
        first, second
    }

    private String originalWord;
    private final String stem;
    private final Map<String, Conjugation> tenses = new HashMap<>();
    private ConjugationForm conjugationForm;

    /**
     * Reads data from the JSON object
     * and fills out the necessary fields.
     *
     * @param obj a JSON object containing at least
     *            the field "normalForm". The field
     *            "notes" is optional
     */
    public IrishVerb(JSONObject obj) {
        super(obj, WordType.verb);
        stem = obj.optString("stem", getStem(getNormalForm()));

        if(conjugationForm == ConjugationForm.first){
            addRequiredTense(obj, PRESENT, FirstPresentIrishVerbTense::new);
            addRequiredTense(obj, IMPERFECT, FirstImperfectIrishVerbTense::new);
            addRequiredTense(obj, PRETERITE, FirstPreteriteIrishVerbTense::new);
            addRequiredTense(obj, FUTURE, FirstFutureIrishVerbTense::new);
            addRequiredTense(obj, CONDITIONAL, FirstConditionalIrishVerbTense::new);
            addRequiredTense(obj, IMPERATIVE, FirstImperativeIrishVerbTense::new);

            addOptionalTense(obj, PRESENT_INDEPENDENT, FirstPresentIrishVerbTense::new);
            addOptionalTense(obj, PRESENT_DEPENDENT, FirstPresentIrishVerbTense::new);
            addOptionalTense(obj, PRESENT_NEGATIVE, FirstPresentIrishVerbTense::new);
            addOptionalTense(obj, PRESENT_HABITUAL, FirstPresentIrishVerbTense::new);

            addOptionalTense(obj, PAST_INDEPENDENT, FirstPreteriteIrishVerbTense::new);
            addOptionalTense(obj, PAST_DEPENDENT, FirstPreteriteIrishVerbTense::new);
            addOptionalTense(obj, PAST_HABITUAL, FirstImperfectIrishVerbTense::new);

            addOptionalTense(obj, FUTURE_INDEPENDENT, FirstFutureIrishVerbTense::new);
            addOptionalTense(obj, FUTURE_DEPENDENT, FirstFutureIrishVerbTense::new);

            addOptionalTense(obj, CONDITIONAL_INDEPENDENT, FirstConditionalIrishVerbTense::new);
            addOptionalTense(obj, CONDITIONAL_DEPENDENT, FirstConditionalIrishVerbTense::new);
        }
        else {
            addRequiredTense(obj, PRESENT, SecondPresentIrishVerbTense::new);
            addRequiredTense(obj, IMPERFECT, SecondImperfectVerbTense::new);
            addRequiredTense(obj, PRETERITE, SecondPreteriteIrishVerbTense::new);
            addRequiredTense(obj, FUTURE, SecondFutureIrishVerbTense::new);
            addRequiredTense(obj, CONDITIONAL, SecondConditionalIrishVerbTense::new);
            addRequiredTense(obj, IMPERATIVE, SecondImperativeIrishVerbTense::new);

            addOptionalTense(obj, PAST_INDEPENDENT, SecondPreteriteIrishVerbTense::new);
            addOptionalTense(obj, PAST_DEPENDENT, SecondPreteriteIrishVerbTense::new);
        }
    }

    private String prefix;
    private static final String[] prefixes = new String[]{
            "ais",
            "ath",
            "réamh",
            "moin"
    };

    protected String getStem(String normalForm) {
        String normalFormWithoutPrefix = normalForm;
        for(String prefix : prefixes){
            if(normalForm.startsWith(prefix)){
                this.prefix = prefix;
                normalFormWithoutPrefix = normalForm.replaceFirst(prefix, "");
                originalWord = IrishLenition.removeLenition(normalFormWithoutPrefix);
                break;
            }
        }

        if(countSyllables(normalFormWithoutPrefix) == 1 || normalForm.matches(".*(áin|eáil|eáid)$")) {
            conjugationForm = ConjugationForm.first;
            return normalForm; //.replaceFirst("igh$", "");
        }
        else {
            conjugationForm = ConjugationForm.second;
            return replaceVowelsFromLastSyllable(normalForm);
                    //.replaceFirst("(igh|aigh|ail|is)$", "");
        }
    }


    private Map<String, Conjugation> getTenses(){
        return tenses;
    }

    private void addRequiredTense(JSONObject obj, String key, BiFunction<IrishVerb, JSONObject, Conjugation> func){
        if(obj.has(key)) {
            Conjugation conjugation = func.apply(this, obj.getJSONObject(key));
            if(conjugation.has())
                tenses.put(key, func.apply(this, obj.getJSONObject(key)));
        }
        else
            tenses.put(key, func.apply(this, new JSONObject()));
    }

    private void addOptionalTense(JSONObject obj, String key, BiFunction<IrishVerb, JSONObject, Conjugation> func){
        if(obj.has(key)) {
            Conjugation conjugation = func.apply(this, obj.getJSONObject(key));
            if(conjugation.has())
                tenses.put(key, func.apply(this, obj.getJSONObject(key)));
        }
    }

    private static final String  regexVowels = "(aío|aoi|aoú|ia(i|)|ua(i|)|eái|ea(i|)|eo(i|)|uío|uói|iái|iói|iúi|uái|oío|" +
            "ae(i|)|ái|ai|ao|aí|éa|eá|ei|éi|ío|" +
            "uó|oi|iá|ió|iú|io|uí|úi|iu|uá|ói|oí|ui|á|a|e|é|i|í|o|ó|u|ú)";

    private long countSyllables(String normalForm){
        return compile(regexVowels).matcher(normalForm).results().count();
    }

    private static String replaceVowelsFromLastSyllable(String text) {
        MatchResult result = null;
        for (Iterator<MatchResult> it = Pattern.compile(regexVowels).matcher(text).results().iterator(); it.hasNext(); ) {
            result = it.next();
        }
        if(result == null)
            return text;

        String base = text.substring(0, result.start());
        String end = text.substring(result.end());
        if(end.matches("(dh|gh)"))
            return base;
        return base + end;
    }

    @Override
    public JSONObject toJson() {
        return super.toJson()
                .put("conjugationForm", conjugationForm);
    }

    @Override
    protected JSONObject getConjugations() {
        JSONObject obj =  new JSONObject();

        // If we are in a dictionary we can resolve the tenses
        // that are based on a different base word
        if (isInDictionary() && originalWord != null) {
            List<Word> words = getDictionary().getWords(originalWord, WordType.verb);
            if(words != null){
                IrishVerb verb = (IrishVerb) words.get(0);
                getTenses().clear();
                for(String tense : verb.getTenses().keySet())
                    getTenses().put(tense, verb.getTenses().get(tense));
            }
        }

        // Loop through all the tenses
        for (String tenseStr : tenses.keySet()){
            JSONObject tense = tenses.get(tenseStr).toJson();

            // if prefix is not null, add the prefix to all
            // strings
            if(prefix != null) {
                for (String key : tense.keySet()) {
                    JSONArray words = tense.getJSONArray(key);
                    JSONArray newWords = new JSONArray();
                    for (int i = 0; i < words.length(); i++) {
                        newWords.put(prefix + words.getString(i));
                    }
                    tense.put(key, newWords);
                }
            }

            // Add the tense to the object
            obj.put(tenseStr, tense);
        }

        return obj;
    }

    public String getStem() {
        return stem;
    }

    @Override
    public List<String> getVersions() {
        var list = super.getVersions();
        for(Conjugation conjugation : tenses.values())
            list.addAll(conjugation.getVersions());
        return list;
    }
}
