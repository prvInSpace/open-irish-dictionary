package cymru.prv.dictionary.irish;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.Inflection;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONObject;

import java.util.List;


/**
 * Represents an adposition in Irish
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class IrishAdposition extends Word {

    private final Inflection inflections;

    public IrishAdposition(JSONObject obj) {
        super(obj, WordType.adposition);
        inflections = new Inflection(obj);
    }

    @Override
    protected JSONObject getInflections() {
        return inflections.toJson();
    }

    @Override
    public List<String> getVersions() {
        var list = super.getVersions();
        if(inflections != null)
            list.addAll(inflections.getVersions());
        return list;
    }
}
