package cymru.prv.dictionary.irish;

import cymru.prv.dictionary.common.json.Json;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents a noun case pair in Irish
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class NounCase {

    private List<String> singluars;
    private List<String> plurals;

    public NounCase(JSONObject caseObj){
        singluars = Json.getStringList(caseObj, "singular");
        plurals = Json.getStringList(caseObj, "plural");
    }

    public JSONObject toJson(){
        return new JSONObject()
            .put("singular", singluars)
            .put("plural", plurals);
    }

    List<String> getVersions(){
        List<String> all = new LinkedList<>(singluars);
        all.addAll(plurals);
        return all;
    }

}
